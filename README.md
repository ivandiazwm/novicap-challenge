# NoviCap code challenge

*This is the repository for a NoviCap interview code challenge.*

In this project, we have a store and we want to calculate the total price of the items in a shopping cart
with some special cases.

To run the example in `index.js`
```
npm start
```

To run the tests
```
npm test
```


## Class Structure

**Product** defines a product with three properties: `name`, `code` and `price`

**MugProduct**, **TShirtProduct**, **VoucherProduct** classes are specific for each product.

**DefaultProductFactory** is a factory classes that returns the correct product instance based on its `code`.

---

**PriceRule** determines how the total price of items will be calculated using the method `getTotal`. By default it's just the sum of prices.

**PriceRuleDecorator** is an implementation of a decorator pattern for price rules.

**DiscountManyTShirtsPriceRuleDecorator** is a decorator that discounts `1` for each T-Shirt if there are more than 2 T-Shirts.

**OneFreeVoucherPriceRuleDecorator** is a decorator that puts a discounts a *Voucher* for each two.

---

**Checkout** class indicates a shopping cart. The class consumes a ProductFactory for instantiating products and a *PricingRule* for calculating the total price.
It manages the list of items that are currently scanned.

## Usage example

```
let productFactory = new DefaultProductFactory(); // use a default factory
let priceRule = new PriceRule(); // use a default price rule

// add some price rules
priceRule = new OneFreeVoucherPriceRuleDecorator(priceRule);
priceRule = new DiscountManyTShirtsPriceRuleDecorator(priceRule);

// create cart with factory and rules.
const co = new Checkout(productFactory, priceRule);

// scan the items by code
co.scan("VOUCHER");
co.scan("VOUCHER");
co.scan("TSHIRT");

co.getTotal(); // Returns final price
```

## How to add more functionality
Let's suppose we want to need to add a special tax for each Mug.

We could define a decorator as follows:
```
class MugTaxPriceRuleDecorator extends PriceRuleDecorator {

    getTotal(items) {
        const mugTax = 0.15;
        const mugMatches = items.reduce((total, item) => total + (item.code === MugrProduct.CODE), 0);

        return this.decoratedRule.getTotal(items) + mugMatches * mugTax * MugProduct.PRICE;
    }
}
```

Then we will implement it like this:
```
priceRule = new MugTaxPriceRuleDecorator(priceRule);
```