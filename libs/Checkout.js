class Checkout {
    constructor(productFactory, priceRule) {
        this.items = [];
        this.productFactory = productFactory;
        this.priceRule = priceRule;
    }

    scan(code) {
        this.items.push(this.productFactory.getProduct(code));
    }

    getTotal() {
        return this.priceRule.getTotal(this.items);
    }
}

module.exports = Checkout;