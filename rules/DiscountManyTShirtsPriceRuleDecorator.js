const PriceRuleDecorator = require('./PriceRuleDecorator');
const TShirtProduct = require('../products/TShirtProduct');

class DiscountManyTShirtsPriceRuleDecorator extends PriceRuleDecorator {

    getTotal(items) {
        const tShirtMatches = items.reduce((total, item) => total + (item.code === TShirtProduct.CODE), 0);
        const discount = (tShirtMatches >= 3) ? tShirtMatches : 0;

        return this.decoratedRule.getTotal(items) - discount;
    }
}

module.exports = DiscountManyTShirtsPriceRuleDecorator;