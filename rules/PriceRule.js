class PriceRule {

    getTotal(items) {
        return items.reduce((total, item) => total + item.getPrice(), 0);
    }
}

module.exports = PriceRule;