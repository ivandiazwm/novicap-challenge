const PriceRuleDecorator = require('./PriceRuleDecorator');
const VoucherProduct = require('../products/VoucherProduct');

class OneFreeVoucherPriceRuleDecorator extends PriceRuleDecorator {

    getTotal(items) {
        const voucherMatches = items.reduce((total, item) => total + (item.code === VoucherProduct.CODE), 0);

        return this.decoratedRule.getTotal(items) - Math.floor(voucherMatches / 2) * VoucherProduct.PRICE;
    }
}

module.exports = OneFreeVoucherPriceRuleDecorator;