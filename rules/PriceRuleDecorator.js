const PriceRule = require('./PriceRule');

class PriceRuleDecorator extends PriceRule {
    constructor(decoratedRule) {
        super();
        this.decoratedRule = decoratedRule;
    }
}

module.exports = PriceRuleDecorator;