const assert = require('assert');
const Checkout = require('./libs/Checkout');

const DefaultProductFactory = require('./products/DefaultProductFactory');

const DiscountManyTShirtsPriceRuleDecorator = require('./rules/DiscountManyTShirtsPriceRuleDecorator');
const OneFreeVoucherPriceRuleDecorator = require('./rules/OneFreeVoucherPriceRuleDecorator');
const PriceRule = require('./rules/PriceRule');

let productFactory = new DefaultProductFactory();
let priceRule = new PriceRule();

priceRule = new OneFreeVoucherPriceRuleDecorator(priceRule);
priceRule = new DiscountManyTShirtsPriceRuleDecorator(priceRule);

function asssertCheckout(items, result) {
    const co = new Checkout(productFactory, priceRule);

    items.forEach(itemCode => co.scan(itemCode));

    assert.equal(result, co.getTotal());
    console.log('OK');
}

asssertCheckout(['VOUCHER', 'TSHIRT', 'MUG'], 32.50);
asssertCheckout(['VOUCHER', 'TSHIRT', 'VOUCHER'], 25.00);
asssertCheckout(['TSHIRT', 'TSHIRT', 'TSHIRT', 'VOUCHER', 'TSHIRT'], 81.00);
asssertCheckout(['VOUCHER', 'TSHIRT', 'VOUCHER', 'VOUCHER', 'MUG', 'TSHIRT', 'TSHIRT'], 74.50);
