const Checkout = require('./libs/Checkout');

const DefaultProductFactory = require('./products/DefaultProductFactory');

const DiscountManyTShirtsPriceRuleDecorator = require('./rules/DiscountManyTShirtsPriceRuleDecorator');
const OneFreeVoucherPriceRuleDecorator = require('./rules/OneFreeVoucherPriceRuleDecorator');
const PriceRule = require('./rules/PriceRule');

let productFactory = new DefaultProductFactory();
let priceRule = new PriceRule();

priceRule = new OneFreeVoucherPriceRuleDecorator(priceRule);
priceRule = new DiscountManyTShirtsPriceRuleDecorator(priceRule);

const co = new Checkout(productFactory, priceRule);
co.scan('VOUCHER');
co.scan('TSHIRT');
co.scan('VOUCHER');
co.scan('VOUCHER');
co.scan('MUG');
co.scan('TSHIRT');
co.scan('TSHIRT');
console.log(co.getTotal());
