class Product {
    constructor({code, name, price}) {
        this.code = code;
        this.name = name;
        this.price = price || 0;
    }

    getPrice() {
        return this.price;
    }

    getCode() {
        return this.code;
    }

    getName() {
        return this.name;
    }
}

module.exports = Product;