const Product = require('./Product');

class MugProduct extends Product {
    constructor() {
        super({code: MugProduct.CODE, name: 'NoviCap Coffee Mug', price: MugProduct.PRICE});
    }
}

MugProduct.CODE = 'MUG';
MugProduct.PRICE = 7.50;

module.exports = MugProduct;