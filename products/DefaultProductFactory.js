const Product = require('./Product');

const VoucherProduct = require('./VoucherProduct');
const MugProduct = require('./MugProduct');
const TShirtProduct = require('./TShirtProduct');

class DefaultProductFactory {

    getProduct(code, props = {}) {
        switch(code) {
            case VoucherProduct.CODE:
                return new VoucherProduct();
            case MugProduct.CODE:
                return new MugProduct();
            case TShirtProduct.CODE:
                return new TShirtProduct();
            default:
                return new Product(props);
        }
    }
}

module.exports = DefaultProductFactory;