const Product = require('./Product');

class TShirtProduct extends Product {
    constructor() {
        super({code: TShirtProduct.CODE, name: 'NoviCap T-Shirt', price: TShirtProduct.PRICE});
    }
}

TShirtProduct.CODE = 'TSHIRT';
TShirtProduct.PRICE = 20.00;

module.exports = TShirtProduct;