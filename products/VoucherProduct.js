const Product = require('./Product');

class VoucherProduct extends Product {
    constructor() {
        super({code: VoucherProduct.CODE, name: 'NoviCap Voucher', price: VoucherProduct.PRICE});
    }
}

VoucherProduct.CODE = 'VOUCHER';
VoucherProduct.PRICE = 5.00;

module.exports = VoucherProduct;